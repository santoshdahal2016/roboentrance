
<?php
require_once("include/initialize.php");

if($session->is_logged_in()) {
    redirect_to("index.php");
}

// Remember to give your form's submit tag a name="submit" attribute!
if (isset($_POST['submit'])) { // Form has been submitted.

    $username = trim($_POST['username']);
    $password = trim($_POST['password']);

    // Check database to see if username/password exist.
    $found_user = User::authenticate($username, $password);

    if ($found_user) {
        $session->login($found_user);
        redirect_to("index.php");
    } else {
        // username/password combo was not found in the database
        $message = "Username/password combination incorrect.";
    }

} else { // Form has not been submitted.
    $username = "";
    $password = "";
}

?>
<?php include_once'include/header.php';
?>

<?php



echo<<<here
<div class="container">
    <div class="row vertical-offset-100">
        <div class="col-md-4 col-md-offset-4">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Please login in</h3>
                </div>
                <div class="panel-body">
                    <form accept-charset="UTF-8" role="form" action="login.php" method="post">
                        <fieldset>
                            <div class="form-group">
                                <input class="form-control" placeholder="username" name="username" type="text">
                            </div>
                            <div class="form-group">
                                <input class="form-control" placeholder="Password" name="password" type="password" value="">
                            </div>

                            <input class="btn btn-lg btn-success btn-block" name="submit" type="submit" value="Login">
                            <a class="btn btn-lg btn-alert btn-block"  style="color:black;" href="$url/signup.php" >Find Your username and password</a>

                        </fieldset>
                    </form>
                </div>
            </div>
            <div style="color:black;" class="panel panel-success">$message</div>
        </div>
    </div>
</div>

    
here;


?>

<?php include_once 'include/footer.php'; ?>
 
  



 
 
