<?php

include_once 'constant.php';
include_once 'connect.php';


function strip_zeros_from_date( $marked_string="" ) {
    // first remove the marked zeros
    $no_zeros = str_replace('*0', '', $marked_string);
    // then remove any remaining marks
    $cleaned_string = str_replace('*', '', $no_zeros);
    return $cleaned_string;
}

function redirect_to( $location = NULL ) {
    if ($location != NULL) {
        header("Location: {$location}");
        exit;
    }
}

function output_message($message="") {
    if (!empty($message)) {
        return "<p class=\"message\">{$message}</p>";
    } else {
        return "";
    }
}

function __autoload($class_name) {
    $class_name = strtolower($class_name);
    $path = LIB_PATH.DS."{$class_name}.php";
    if(file_exists($path)) {
        require_once($path);
    } else {
        die("The file {$class_name}.php could not be found.");
    }
}

function include_layout_template($template="") {
    include(SITE_ROOT.DS.'public'.DS.'layouts'.DS.$template);
}


function get_setting($name ,$conn){

    $query = "SELECT *
				FROM configuration ";
    $query .= "WHERE name = '$name' ";
    $query .= "LIMIT 1";

    $result = $conn->query($query);
    $row = $result->fetch_assoc();
    return $row['status'];

}
function get_random_quote($conn){

    $query = "SELECT quote FROM quote ";
    $query .= "ORDER BY rand() ";

    $result = $conn->query($query);
    return $result;

}

// This file is the place to store all basic functions
function totalno_by_subject($subject)
{
    $sql = "SELECT * FROM `question` WHERE subject = $subject ";


    $resul =  mysqli_query($sql);
    while ($row = mysqli_fetch_assoc($resul))
    {

        $idarray []  = $row['id'];

    }
    $qno = count ( $idarray ) ;
    return $qno ;
}
function totalno_by_subject_chapter($subject,$chapter)
{
    $sql = "SELECT * FROM `question` WHERE subject = $subject AND chapter = $chapter  ";


    $resul =  mysqli_query($sql);
    while ($row = mysqli_fetch_assoc($resul))
    {

        $idarray []  = $row['id'];

    }
    $qno = count ( $idarray ) ;
    return $qno ;
}

function get_student_record($user)
{
    $query = "SELECT * FROM user where user ='$user' ";



    $result_set = mysqli_query($query);
    confirm_query($result_set);
    $subject = mysqli_fetch_array($result_set);
    return $subject ;



}
function get_questions_by_faculty($value , $conn)
{
    $query = "SELECT * 
				FROM question ";
    $query .= "WHERE faculty = {$value} ";
    $query .= "ORDER BY id ASC ";


    $result = $conn->query($query);
    return $result;



}
function get_questions_by_subject($value){
    $query = "SELECT * 
				FROM question ";
    $query .= "WHERE subject = {$value} ";


    $questions_set = mysqli_query($query);
    confirm_query($questions_set);
    return $questions_set;



}
function get_questions_by_subject_and_chapter($subject,$chapter){
    $query = "SELECT * 
				FROM question ";
    $query .= "WHERE subject = {$subject} AND chapter = {$chapter} ";


    $questions_set = mysqli_query($query);
    confirm_query($questions_set);
    return $questions_set;



}
function get_questions_by_subject_and_chapter_and_difficulty($subject,$chapter,$diff){
    $query = "SELECT * 
				FROM question ";
    $query .= "WHERE subject = {$subject} AND chapter = {$chapter} AND difficulty = {$diff} ";


    $questions_set = mysqli_query($query);
    confirm_query($questions_set);
    return $questions_set;

    function get_questions_by_nosetfield($field){
        $query = "SELECT * 
				FROM question ";
        $query .= "WHERE  {$subject} = null  ";

        $query .= "ORDER BY position ASC";
        $questions_set = mysqli_query($query);
        confirm_query($questions_set);
        return $questions_set;



    }

}
function mysqli_prep( $value ) {
    $magic_quotes_active = get_magic_quotes_gpc();
    $new_enough_php = function_exists( "mysqli_real_escape_string" ); // i.e. PHP >= v4.3.0
    if( $new_enough_php ) { // PHP v4.3.0 or higher
        // undo any magic quote effects so mysqli_real_escape_string can do the work
        if( $magic_quotes_active ) { $value = stripslashes( $value ); }
        $value = mysqli_real_escape_string( $value );
    } else { // before PHP v4.3.0
        // if magic quotes aren't already on then add slashes manually
        if( !$magic_quotes_active ) { $value = addslashes( $value ); }
        // if magic quotes are active, then the slashes already exist
    }
    return $value;
}




function confirm_query($result_set) {
    if (!$result_set) {
        die("Database query failed: " . mysqli_error());
    }
}

function get_all_subjects($public = true) {

    $query = "SELECT * 
				FROM subject ";
    if ($public) {
        $query .= "WHERE visible = 1 ";
    }
    $query .= "ORDER BY position ASC";
    $subject_set = mysqli_query($query);
    confirm_query($subject_set);
    return $subject_set;
}

function get_chapter_for_subject($subject_id) {

    $query = "SELECT * 
				FROM chapter ";
    $query .= "WHERE subject = {$subject_id} ";

    $query .= "ORDER BY position ASC";
    $chapter_set = mysqli_query($query);
    confirm_query($page_set);
    return $chapter_set;
}

function get_subject_by_id($subject_id) {

    $query = "SELECT * ";
    $query .= "FROM subject ";
    $query .= "WHERE id=" . $subject_id ." ";
    $query .= "LIMIT 1";
    $result_set = mysqli_query($query);
    confirm_query($result_set);
    // REMEMBER:
    // if no rows are returned, fetch_array will return false
    if ($subject = mysqli_fetch_array($result_set)) {
        return $subject;
    } else {
        return NULL;
    }
}

function get_chapter_by_subject($value) {

    $query = "SELECT * ";
    $query .= "FROM chapter ";
    $query .= "WHERE subject=" . $value ." ";

    $result_set = mysqli_query($query);
    confirm_query($result_set);
    return $result_set;

}
function get_question_by_id($question_id , $conn) {

    $query = "SELECT * ";
    $query .= "FROM question ";
    $query .= "WHERE id=" . $question_id ." ";
    $query .= "LIMIT 1";
    $result = $conn->query($query);
    return $result->fetch_assoc();

}
function get_subject_row($subjectid){
    $query = "SELECT * ";
    $query .= "FROM subject ";
    $query .= " WHERE id = {$subjectid}  "."";
    $query .= "LIMIT 1";

    $result_set = mysqli_query($query);
    confirm_query($result_set);
    $subject = mysqli_fetch_array($result_set);
    return $subject ;

}
function get_chapter_row($subjectid , $chapterid){
    $query = "SELECT * ";
    $query .= "FROM chapter ";
    $query .= " WHERE subject = {$subjectid} AND chapterid = {$chapterid} ";

    $result_set = mysqli_query($query);
    confirm_query($result_set);
    $subject = mysqli_fetch_array($result_set);
    return $subject ;


}



?>