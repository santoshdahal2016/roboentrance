<?php

// Define the core paths
// Define them as absolute paths to make sure that require_once works as expected

// DIRECTORY_SEPARATOR is a PHP pre-defined constant
// (\ for Windows, / for Unix)
defined('DS') ? null : define('DS', DIRECTORY_SEPARATOR);

defined('SITE_ROOT') ? null :
    define('SITE_ROOT', DS.'home'.DS.'bomrty'.DS.'Projects'.DS.'ROBOTICS'.DS.'entrance');

defined('LIB_PATH') ? null : define('LIB_PATH', SITE_ROOT.DS.'include');

// load config file first
// require_once(LIB_PATH.DS.'config.php');
require_once('config.php');

// load basic functions next so that everything after can use them
require_once('function.php');

// load core objects
require_once('session.php');
require_once('database.php');
//require_once(LIB_PATH.DS.'database_object.php');

// load database-related classes
require_once('user.php');

?>