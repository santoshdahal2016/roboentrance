
<?php
include_once 'include/header.php';
?>

<!-- This is a very simple parallax effect achieved by simple CSS 3 multiple backgrounds, made by http://twitter.com/msurguy -->

<div class="container">
    <div class="row vertical-offset-100">
        <div class="col-md-4 col-md-offset-4">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Sign Up</h3>
                </div>
                <div class="panel-body">
                    <form accept-charset="UTF-8" role="form" action="<?php echo $url ?>/detail.php" method="post">
                        <fieldset>
                            <div class="form-group">
                                <input class="form-control" placeholder="BEX439" name="roll_no" type="text">
                            </div>
                            <div class="form-group">
                                <input class="form-control" placeholder="2053/12/01" name="bod" type="text" value="">
                            </div>

                            <input class="btn btn-lg btn-success btn-block" type="submit" value="Submit">
                            <a  style="color:black;" class="btn btn-lg btn-alert btn-block" href="<?php echo $url ?>/login.php" >Login</a>

                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<?php include_once 'include/footer.php'; ?>