<?php
require_once("include/initialize.php");

if ($session->is_logged_in()){
    $user = User::find_by_id($session->user_id);


    if ($user->is_admin()){

        header('location:'.$url.'/admin');
    }
    else{
        header('location:'.$url.'/exam.php');
    }
}
else
    { redirect_to("login.php"); }


